---
title: "About author"
date: 2020-04-04T22:00:26+05:30
draft: false
---

Anuradha Priyankara
---

{{< figure src="../posts/img/about/profile_pic.jpg">}}

I'm an Electronic Engineering student of University of Moratuwa, Sri Lanka currently awaiting gaduation. I'm interested in Embedded Systems, Digital Design, Robotics, IoT, Networking and many other things which involves electronic hardware. Currently I'm employed in electronic design industry. I do electronics as a hobby as well as a living. I'm also available for custom design services. Just ring me a bell via any of the contacts listed :-;

I live in Sri Lanka with my parents, my brother and my 3 cats. I like to read books, play video games and watch movies when I'm not working.
