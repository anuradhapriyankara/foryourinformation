---
title: "Make a Wall following, Arrow following and Maze solving Robot with Vertical Lift Arm using Arduino"
tags : [
    "arduino",
    "wall following robot",
    "maze solving",
    "vertical lift arm",
    "arrow following robot",
]
description : "Build an arduino powered Wall following, Arrow following and Maze solving Robot with Vertical Lift arm"
date: 2020-04-07T09:13:17+05:30
draft: false
---
**Background of the project**
---

Back in 2017 me and some of my friends decided to take part in a robotic compettition which involved building a Robot which can Follow Walls, Solve a Maze, Follow Arrows and Carry a payload. The play arena was consist of a Wall Maze and a area with color arrows on the floor. First, the robot needed to solve the wall maze from the starting point and find the exit. Then there was a Cube at the exit of the maze which the robot needed to identify and carry as a payload. The robot needed to identify the color of the payload and continue to the next part. The next part had multiple color arrows on the floor. The robot would follow the arrows which had a same color of the payload. At the end of arrow path, there was an unloading area to unload the payload. The challenge of the competition was completing all these task in least amount of time. Here is the play arena,
{{< figure src="../img/robot/play_arena.png" caption="Play Arena" >}}
We have designed a robot and competed in the competition. We were able to make it to the second round but sadly could not win any places due to design flaws and some bad luck. Anyway it was a great experience and we learned lots from the project. Now I have decided to share the project design materials with you with my new website. Hope this will be useful for you to build your own robot.

**Design of the Robot**
---

{{< figure src="../img/robot/robot_front.png" caption="Almost finished robot" >}}

**Chasis**

We have used 3 mm thick transparent acrylic sheets to make the chasis of the robot and applied a carbon fiber textured Vinyl Sticker on it to make it beautiful. The design of the robot was done using Autodesk AutoCAD software and we cut the acrylic sheet using a laser cutter. The parts were put together by simply fitting but sometimes we used super glue and hot glue as well. Here is the final design of the CAD file. You can [download the Autocad project from here.](https://github.com/anuradhapriyankara/maze-solving-robot/tree/master/Chasis/AutoCAD)

{{< figure src="../img/robot/chasis_design.png" caption="Chasis Design" >}}

It may be looks like a whole jigsaw puzzle for you. I have no idea how can I instruct you to assemble it since I did not make any 3D model for it. So it's up to you to put it together. You can find some pictures of the chasis from various angles [from here.](https://github.com/anuradhapriyankara/maze-solving-robot/tree/master/Pictures) I guess it will help.

**Vertical Lift Arm**

Vertical lift arm is the most iconic part of our robot. We have used [Rack and Pinion Mechanism](https://en.wikipedia.org/wiki/Rack_and_pinion) to move the arm platform vertically. The rack is on the chasis of the robot and pinion is attached to a motor mounted on the platform. We have used a 100 rpm non-encorder motor for that. Hence we had to use two limit switches attached on upper and lower ends of the platform moving space in order to stop motor once platform reached upper or lower position. You can use an encoder motor if you want to precisely control the position of the platform. We have used a [MG996R Servo Motor](https://www.towerpro.com.tw/product/mg996r/) to control the grabbing arm of the platform.

Parts required for the project
---

**Motors**

2 x 12V, 300 rpm, encoder motors (For locomotion)

1 x 12V, 100 rpm, non-encoder motor (For arm platform)

1 x [MG996R Servo Motor](https://www.towerpro.com.tw/product/mg996r/) (For grabber arm)

I don't have a link for the encoder motors since we bought it from local market. They were cheap Chinese motors without any brand name. The model number was JGA25. I think [this](https://www.seeedstudio.com/JGA25-370-Geared-Motor-p-4119.html) motor is a similar one.

**Motor Drivers**

2 x [ VNH2SP30 Monster Moto Shield ](https://www.sparkfun.com/products/10182)

Each one one these can control 2 motors. We have used one driver for locomotion motors and other for the arm platform motor.

**Microcontrollers**

1 x [Arduino Mega 2560](https://store.arduino.cc/usa/mega-2560-r3)

We have used Arduino Mega 2560 board to control our robot. The code is developed using arduino as well.

**Sensors**

5 x HC-SR04 Ultrasonic Sensors (For wall following and maze solving)

2 x TCS 3200 Color Sensors (For detecting colors of the arrows and the payload)

10 x LDRs (For detecting arrows)

10 x UV LEDs (For arrow illuminating arrows)

2 x Limit Switches (For positioning vertical arm)

**Power**

1 x 5V 3A Buck converter (For power supply of Arduino and sensors)

1 x Custom build power distribution board

1 x 14.8 V, 2200 mAh Li-Po battery

**Miscellaneous**

2 x Robot Wheels

2 x Hex nuts

2 x Caster Balls (For front wheels)

1 x 20A Power Switch

5 x ON/OFF Switches (For selecting different modes)

1 x Push Button (Global Reset)

Jumper wires

Power wires

Spacers

Solder

Screws, Nut and Bolt (3 mm)

Hot glue

Resistors - Multiple Values(For pullup/down, current limitng)

LEDs

**Wiring Digram**

{{< figure src="../img/robot/schematic_robot.png" >}}

I have used Fritzing to create a schematic for the robot. I don't use Fritzing for anything but I specially made this since it may be easy to you to do the wiring rather looking into code. Please note that none of the components are connected to Power nor Ground in order to reduce shematic complexity. You may connect those in your build. You can also [downlaod Fritzing file from here.](https://github.com/anuradhapriyankara/maze-solving-robot/tree/master/Schematic)

**Arrow Following**

The arrow following is done by a LDR sensor panel and a Color sensor. The color sensor is used to identify the color of the arrow and LDR panel is used to identify the arrows. We have used UV (Ultraviolet) LEDs to illuminate arrows since it gave the best contrast. There are Two sensor panels mounted under the robot but we have only used one. Here is a picture under the robot. You can see how Sensor Panels and color sensor is mounted. You can download [PCB design of the sensor panel from here.](https://github.com/anuradhapriyankara/maze-solving-robot/tree/master/PCB)

{{< figure src="../img/robot/robot_under.png" >}}
{{< figure src="../img/robot/sensor_panel.png" >}}

**Coding**

We have used Arduino to code our robot. You can download the [entire source code from here.](https://github.com/anuradhapriyankara/maze-solving-robot/tree/master/Codes/Total_Code1.4) The wall following and maze solving part of the robot works perfectly. The arrow following part does not work so well because of the low quality readings from the LDR, interference and algorithm problems. You may improve it at your will.
