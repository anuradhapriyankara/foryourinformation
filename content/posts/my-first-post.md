---
title: "Implementing a 6LoWPAN Wireless Sensor Network"
date: 2020-03-09T19:12:05+05:30
draft: false
---
With the emergence of IoT, wireless sensor networks (WSNs) has become a popular method to monitor and control the enviroment.
There are various wireless protocols available today, to implement such networks. WiFi, Zigbee, 6LoWPAN, LoRa and Nb-IoT can be
listed as some popular technologies.

When it comes to low powered, mesh topology networks, Zigbee and 6LoWPAN are currently popular solutions. Both of these networks
use IEEE 802.15.4 for their Physical and MAC layer protocols. Upper layers are defined by respective protocols. However, Zigbee being
a well standardized protocol still remains expensive to be used for mass deployements. On other hand 6LoWPAN being an open protocol
gives much flexibile and cost effective solutions.

We have developed a 6LoWPAN mesh network as the final year project of my university degree program. I wish to share some of my experience
about implementing such network in this tutorial series. Eventually I hope you will be able to build your own network.

See a video demo of the project here -
{{< youtube Bg00dqLR-5U >}}

{{< figure src="../img/6lointro/network_Architecture_rgb.jpg" caption="6LoWPAN Network Architecture" >}}


