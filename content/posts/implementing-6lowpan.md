---
title: "Implementing 6LoWPAN"
date: 2020-03-16T19:53:54+05:30
draft: false
---
Here we will discuss about implementing the actual 6LoWPAN network using Contiki-os and CC2650/CC1310. I hope you have read the [previous post](www.bitsbytesandintegers.com/posts/contiki-os/) about toolchain installtion and flashing the device. That knowledge is required to continue this tutorial.

Setting up the Border Router
---
Border router is the device which acts as the access point to the 6LoWPAN network and coordinator of the network. This device is called the "PAN Coordinator" in these type of networks. We do not need different hardware for this device. We can implement this device by flashing a Launchpad with an specific firmaware. There are two ways of doing this.

**1. Run border router on Launchpad**

In this mode the entire protocol stack will be running on Launchpad device itself. This will connect to a Linux host using a Serial port and a client software runs on the Linux host will convert the serial interface into a virtual network adapter.

**2. Run border router on Linux host**

In this method the firmware of the Launchpad will make it a simple RF modem and the protocl stack will be running on the client software of Linux host. The client software will connect to the Launchpad using a Serial interface and create a virtual network adpater just as the first method.

We will be using the **Second Method** in this tutorial since it was very conveiniet to use. Follow these steps to set up the border router.

**1. Compile "Slip Radio"**

Go to *./contiki/examples/ipv6/slip-radio* directory and compile the code using following command.

```make TARGET=srf06-cc26xx BOARD=launchpad/cc2650```

Upload the compiled slip-radio.bin to your launchpad. Do not unplug the device!

**2. Compile Native Border Router**

Go to ```./contiki/examples/ipv6/native-border-router``` and compile the code using following command.

```make TARGET=native```

**3. Run border router**

Go to the same directory on above step and run following command. Make sure your Launchpad is connected and flashed with slip-radio. Run this command to launch border router.

```sudo ./border-router.native fd00::1/64 -s /dev/ttyACM0```

/dev/ttyACM0 is the serial port of your border launchpad. If yours have different name for the serial port, replace /dev/ttyACM0 with it. If this client software runs without any errors and does not crash we can expect the border router is configured correctly. Type ```ifconfig``` on your terminal to view network interfaces on your Linux host. If you can see an interface called "tun0", the border router is up and running.

Finding your devices IPv6 address
---

To find the IPv6 address of your device connect your device to USB and open the device using Uniflash. Go to *Settings and Utilities* on the left sidebar. Scroll down to *Primary IEEE Address* and hit *Read*. A MAC address will be displayed something like ```215:4b00:1c98:d8e0```. Copy down this address and add "fd00:" in front of that address.

Example -

If your MAC is ```212:4b00:1c98:d8e0```, your IPv6 will be ```fd00::212:4b00:1c98:d8e0```

Copy down this address. We will need it for the next steps.

Compiling CC2650/CC1310 Web Demo
---

CC2650/CC1310 Web-demo is an example with a whole lot protocol set implemented on a single example. This example consists of HTTP, MQTT, CoAP and NetUART. You can you this example as a base code to develop your own 6LoWPAN applications of CC2650/CC1310 devices. Go to ```./contiki/examples/cc26xx/cc26xx-web-demo``` and compile the code with usual compilation command (Can found in slip-radio compilation step). Flash your device with generated "cc26xx-web-demo.bin" file.

Using CC2650/CC1310 web demo
---
Once you have finished flashing the device reset it and keep connected to power. On launchpad the Green LED will blink for some time and will be turned OFF eventually. This means it has coonected to your border router device. Open a terminal and enter the command

 ```ping6 <your_devices_ipv6_address>```

You have to replace <your_devices_ipv6_address> with the address constructed in a previous step. You may also need to install ping6. If you can get a ping reply your device has successfully connected to 6LoWPAN network.

CoAP Client
---
coap-client is a Linux command line tool based on libcoap to test your CoAP servers. Since there is a CoAP server running on this web-demo example we can use this tool to retrive data from CoAP resources. To install run,

```
sudo apt update
sudo apt-get install libcoap-1-0
```
After installing run, ```coap-client -m get coap://[<your device's ipv6 address>]/.well-known/core``` You have to replace your device's IPv6 as usual. You will get something like following,

{{< figure src="../img/6loimple/coap_response.jpg" caption="Response for well known core resource" >}}

The "well-knowm/core" resource of a CoAP resource shows all available CoAP resources on the CoAP server. You can select any resource you need and request it from the server. For example if you need to get battery voltage run,

```coap-client -m get coap://[<your device's ipv6 address>]/sen/batmon/voltage```

Testing HTTP
---
The CC2650/CC1310 web-demo also provide a HTTP web server in the example. Just open a browser and enter ```[<your device's ipv6 address>]``` A web page will open and show all kinds of readings and settings available.
{{< figure src="../img/6loimple/web_interface.png" caption="http Web interface" >}}
