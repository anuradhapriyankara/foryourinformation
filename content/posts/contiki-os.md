---
title: "The Contiki Operating System"
date: 2020-03-15T15:45:04+05:30
draft: false
---
The Contiki operating system is an operating system for constrained devices such as low power microcontrollers. Contiki OS provides 6LoWPAN TCP/IP, UDP and
various other communication stacks for such devices. The latest branch of this project is called [Contiki-ng](http://www.contiki-ng.org/) which is still developed
today. The older branch [Contiki-OS](http://www.contiki-os.org/) is not being developed anymore but very stable and supported by many devices. We have used the older
branch Contiki-OS for our project since the new Contiki-ng does not support Over the Air Firmware Update, which was required by our project. Hence the example codes
provide in this tutorial will be based on older version ( Contiki-OS ). However new version is not
much different from the older one so you will be able to use most from this tutorial for new version as well.

Setting up Contiki toolchain and development environment for CC2650/CC1310
---
For developing with Contiki we recommend you to use an Linux environment. We have implemented and tested our project based on Ubuntu 18.04 development environment.
It is still possible to compile codes in Windows systems with support of [Cygwin](https://www.cygwin.com/). If you wish to use such method please read [this](#) article.

**1. Install GNU ARM Embedded Toolchain**

Download the Linux tarball of [GNU ARM Embedded Toolchain](https://launchpad.net/gcc-arm-embedded/5.0/5-2015-q4-major). Extract  ```gcc-arm-none-eabi-5_2-2015q4```  folder
to your ```/opt``` folder.

**2. Add ARM Toolchain to your $PATH**

Add following line to your ```/.bashrc```

```export PATH="$PATH:/opt/gcc-arm-none-eabi-5_2-2015q4/bin"```

**3. Clone Contiki-os github repo**

Run ```git clone https://github.com/contiki-os/contiki.git``` to clone Contiki git repo to your computer.


**4. Test the compilation**

Go to ```./contiki/examples/hello-world``` and enter following command. I your are using a CC1310 Launchpad replace cc2650 with cc1310.

```make TARGET=srf06-cc26xx BOARD=launchpad/cc2650```

If you have done everything correct, the code should compile fine and create "hello-world.bin" file on hello-world directory. This file is the firmware which will be programmed into your device. If you stumble upon any errors, check previous steps.

**5. Install Uniflash**

Uniflash is the official tool from Texas Instruments to flash your device with the firmware.

[Download and install Uniflash from here](http://www.ti.com/tool/UNIFLASH)

According to my experience latest versions of Uniflash have some bugs and some chips might not be recognized. You can download a previous version (4.6) from [here](https://processors.wiki.ti.com/index.php/CCS_UniFlash_v4.6.0_Release_Notes) if you happen to have such problems. Also Uniflash need some dependencies to work with.
You will have errors during installation if they are not met. You may need to install them manually in that case.

**6. Install Cutecom**

Cutecom is a serial terminal software you can use to monitor the serial output from your device. You can install it form apt repository of Ubuntu.

```sudo apt-get update```

```sudo apt-get install cutecom```

Now plug in your device and launch Uniflash. If your device not recognized automatically click on "Detect". Hit "Start" once your device is detected. Browse the "hello-world.bin" file from the Step 4 and flash the device. Open Cutecom with sudo permissions and select the port as "ACM0" and set baudrate to 115200. Open the serial port and reset your device by pressing reset button. If "Hello, world" apperas on Cutecom serial monitor, Congratulations! the hard part is over. If you happen to met with any problems check above steps.

We will discuss about implementing the 6LoWPAN network in next articles.
