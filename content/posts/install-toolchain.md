---
title: "Choosing Hardware for 6LoWPAN Network"
date: 2020-03-10T20:00:19+05:30
draft: false
---
Choosing a hardware device to implement your network is the first thing you should consider about. To run a 6LoWPAN network you need to
have an RF Transceiver device which offers IEEE 802.15.4 Physical and MAC layers. Wikipedia has [this page](https://en.wikipedia.org/wiki/Comparison_of_802.15.4_radio_modules) page which list lot of these devices from various manufactureres.
These devices comes with different operational frequencies and various processig powers. You need to consider about few parameters when you choose a device.

**1. Communication Band (Frequency)**

Unlike ZigBee which only operates only at 2.4GHz 6LoWPAN can be run on various ISM bands such as 2.4GHz, 868 MHz, 915 MHz and 433 MHz. Not all of these bands are allowed
in some countries. So you need to check whether which bands are allowed in your country. If you need short range communication and high bandwidth 2.4 GHz is ideal for that.
Also this band is allowed worldwide. If you need long range and better penetration you have to use a Sub 1 GHz frequency.

**2. Processing Power and Pheriperals**

Most of the devices on above list comes with embedded microcontrollers on them. You can use that to run the rest of the protocol stack and any other processing you wish to do.
Also some of these devices offer other pheriperals such as ADC, PWM and Temperature, Volatge sensors. Those will come in handy when you develop a compact IoT device.


**3. Software and Firmware Support**

Even though these devices provide IEEE 802.15.4 Physical and MAC layers you still need to implement the rest of the protocol stack on your own. Since that takes some huge effor,
the best way to do this is using an Open Source software stack or any other stack provided by the manufacturers. Make sure your devide support any of that. For our project
we have used [Contiki OS](https://www.contiki-ng.org/) which is an open source project which supports range of RF Transceivers.

I hope you can select an RF Transceiver for your project refering to above points.

Texas Instruments CC2650
------------------------------
We have used [Texas Instruments CC2650](http://www.ti.com/product/CC2650) for our project. [CC2650 Launchpad](http://www.ti.com/tool/LAUNCHXL-CC2650) has been used as the
development board. Ultimately we have designed and assembled our own CC2650 based device. We have developed and tested our project with CC2650
Launchpad and Custom board. **The rest of the tutorial will be entirely based on that hardware.** However you will still be able to apply some of the techniques discussed here for
other devices with some slight modifications.
{{< figure src="../img/hardware/launchpad.jpg" caption="CC2650 Launchpad" >}}
{{< figure src="../img/hardware/budgetmesh_top.jpg" caption="Custom built device" >}}
