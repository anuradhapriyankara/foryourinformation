---
title: "Getting Neighbour Table as a CoAP Resource in Contiki"
date: 2020-04-05T09:46:58+05:30
draft: false
---
Knowing about the neighbours is a nice to have feature in mesh networks such as 6LoWPAN. But getting the neighbour table as a CoAP resource is not something documented in Contiki documentation. Here I will explain how to get neighbour table information as a CoAP resource in Contiki CC2650/CC1310 Web-demo. You may also be able to use this implementation for other platforms with some small modificarions.

Go to ```./contiki/examples/cc26xx/cc26xx-web-demo/resources``` and open 'res-net.c'. This is the CoAP resource folder which include all the CoAP resources and this file corresponds to the network resources. Add following lines to 'include' section.

```
#include "nbr-table.h"
#include "link-stats.h"
```
Add following function to the function section.

```
static void
res_get_handler_all_neighbours(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  int32_t strpos = 0;

  /* Check the offset for boundaries of the resource data. */
  if(*offset >= CHUNKS_TOTAL) {
    REST.set_response_status(response, REST.status.BAD_OPTION);
    /* A block error message should not exceed the minimum block size (16). */

    const char *error_msg = "BlockOutOfScope";
    REST.set_response_payload(response, error_msg, strlen(error_msg));
    return;
  }

  char ipaddr_buf[MY_IPADDR_BUF_LEN]; /* Intentionally on stack */
  static uip_ds6_nbr_t *m_ip;
  static bool initialized = false;
  linkaddr_t *m_link_addrs;
  const struct link_stats *my_link_stats;

  if(!initialized){
    m_ip = nbr_table_head(ds6_neighbors);
    initialized = true;
  }
  else{
    m_ip = nbr_table_next(ds6_neighbors, m_ip);
  }
  if(m_ip != NULL){
    memset(ipaddr_buf, 0, MY_IPADDR_BUF_LEN);
    cc26xx_web_demo_ipaddr_sprintf(ipaddr_buf, MY_IPADDR_BUF_LEN, &m_ip->ipaddr);

    m_link_addrs = nbr_table_get_lladdr(ds6_neighbors,m_ip);
    my_link_stats = link_stats_from_lladdr(m_link_addrs);
    strpos += snprintf((char *)buffer + strpos, preferred_size - strpos + 1, "[\"%s\",%hu,%d,%hu],", ipaddr_buf, my_link_stats->etx,my_link_stats->rssi,my_link_stats->freshness);
   }

  /* snprintf() does not adjust return value if truncated by size. */
  if(strpos > preferred_size) {
    strpos = preferred_size;
    /* Truncate if above CHUNKS_TOTAL bytes. */
  }
  if(*offset + (int32_t)strpos > CHUNKS_TOTAL) {
    strpos = CHUNKS_TOTAL - *offset;
  }
  REST.set_response_payload(response, buffer, strpos);

  /* IMPORTANT for chunk-wise resources: Signal chunk awareness to REST engine. */
  *offset += strpos;

  /* Signal end of resource representation. */
  if(m_ip==NULL){
    initialized = false;
    *offset = -1;
  }
}
```
Add following lines to the end of the file,

```
/*---------------------------------------------------------------------------*/
RESOURCE(res_neighbour_ips, "title=\"Neighbours \";rt=\"IPv6 addresses\"",
         res_get_handler_all_neighbours, NULL, NULL, NULL);
/*---------------------------------------------------------------------------*/
```

Save everything and close this file. Go to ```./contiki/examples/cc26xx/cc26xx-web-demo/``` and open 'coap-server.c' file. Add following line to 'Common Resources Section'

```
extern resource_t res_neighbour_ips;
```
Add following line inside 'PROCESS_THREAD(coap_server_process, ev, data)'.

```
rest_activate_resource(&res_neighbour_ips, "net/neighbours/IPv6");
```

Compile the code and flash the firmware to the device. Enter following command on terminal of border router host computer (Linux).

```
coap-client -m get coap://[fd00::212:4b00:1c98:d8ba]/net/neighbours/IPv6
```
You will receive a list of IPv6 addresses of the neighbour devices along with RSSI, ETX and Freshness.
